exports.jobs = [
  {
    "id": 1,
    "title": "Développeur JavaScript",
    "company": "AT Sure",
    "city": "Rennes",
    "description": "Arrogance Technology est une société à taille humaine, leader sur le marché, nous cherchons un développeur Front connaissant TOUS les frameworks y compris ceux qui ne sont pas encore sortis. Si vous parlez Anglais, Allemand et Espagnol et avez des notions de Mandarin, c'est un plus.",
    "contract": "CDI",
    "salary": 25000,
    "area": "pas de déplacement",
    "publishdate": "2017-02-17"
  },
  {
    "id": 2,
    "title": "Développeur Fullstack",
    "company": "c-noo-les-meilleurs",
    "city": "St Malo",
    "description": "Vous maitrisez Angular, Node et MongoDB : ne cherchez plus, c'est nous les vrais leaders sur le marché. Vous souhaitez intégrer une société qui saura vous faire grandir techniquement et humainement, ne cherchez plus (bis) : ça n'existe pas ! (attends t'es sûr qu'on peut le dire). Pour info, nous ne cherchons pas vraiment, c'est juste pour donner l'impression à nos concurrents que nous sommes en très forte croissance.",
    "contract": "CDI",
    "salary": 45000,
    "area": "national",
    "publishdate": "2017-06-04"
  },
  {
    "id": 3,
    "title": "Développeur Angular",
    "company": "SHT",
    "city": "Nantes",
    "description": "Steak Haché Technologie cherche son expert pas cher : chez nous, le minimum c'est déjà trop. Alors envoie ton CV et tes prétentions, mais surtout le CV.",
    "contract": "CDI",
    "salary": 24000,
    "area": "europe",
    "publishdate": "2017-05-07"
  },
  {
    "id": 4,
    "title": "Développeur Angular",
    "company": "Palindrome",
    "city": "Laval",
    "description": "'Engage le jeu que je le gagne' : c'est ce que tu pourras déclarer si tu maîtrises React, Redux et Node. Si en plus tu sais réparer un 'radar nu' et que tu habites sur 'un roc cornu', le poste est fait pour toi. On a déménagé à Laval pour aller au bout de notre obsession. Pas de babyfoot chez nous : on fait ... du Kayak :)",
    "contract": "CDI",
    "salary": 28000,
    "area": "Ouest",
    "publishdate": "2017-06-02"
  }
]