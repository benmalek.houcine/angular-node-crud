const express = require('express');
const app = express();
const bodyParser = require('body-parser');
let data = require('./jobs');
// console.log('jobs : ', data.jobs);

let jobs = data.jobs;

const getAllJobs = () => {
  return jobs;
}


app.use(bodyParser.json());

app.use((req, res, next) => {
  res.header('Access-Control-Allow-Origin', '*');
  res.header('Access-Control-Allow-Headers', 'Content-Type');
  res.header('Access-Control-Allow-Methods', 'DELETE'); 
  next();
});

const api = express.Router();

/*** Root ***/
// GET
api.get('/jobs', (req, res) => {
  res.json(jobs);
});

api.get('/job/:id', (req, res) => {
  const id = parseInt(req.params.id, 10);
  const job = jobs.filter(j => j.id === id);
  if(job.length === 1) {
    res.json({ success: true, job: job[0] });
  } else {
    res.json({ success: false, message: `pas de job ayant pour id ${id}` });
  }
});

// POST
api.post('/job', (req, res) => {
  console.log('************************');
  const job = req.body;
  jobs = [job, ...jobs];
  console.log('total nb of jobs: ' + jobs.length);
  res.json(job);
});

// DELETE
api.delete('/job/:id', (req, res) => {
  const id = parseInt(req.params.id, 10);
  const index = jobs.map(res => res.id).indexOf(id);
  console.log('delete : index -> ' + index);
  if(index !== -1) {
    jobs.splice(index, 1); 
    console.log('total nb of jobs: ' + jobs.length);
    res.json({ success: true, message: `emploi supprimé avec succès` });
  } else {
    res.json({ success: false, message: `pas de job ayant pour id ${id}` });
  }
});

/*** End Root ***/

app.use('/api', api);

const port = 4205;

app.listen(port, () => {
  console.log(`listening on port ${port}`);
});
