import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

import { map, tap } from 'rxjs/operators';
import { Subject, Observable, of } from 'rxjs';


@Injectable({
  providedIn: 'root'
})
export class JobService {

  jobsSubject = new Subject();

  BASE_URL = 'http://localhost:4205/';

  constructor(private http: HttpClient) { }

  getJobs() {
    return this.http.get(this.BASE_URL + 'api/jobs');
  }

  getJob(id) {
    return this.http.get(this.BASE_URL + `api/job/${id}`);
  }

  addJob(jobData) {
    jobData.id = Date.now();

    return this.http.post(this.BASE_URL + 'api/job', jobData).pipe(
      tap(res => {
        this.jobsSubject.next(jobData);
      })
    );
  }

  deleteJob(id) {
    return this.http.delete(this.BASE_URL + `api/job/${id}`);
  }

}
