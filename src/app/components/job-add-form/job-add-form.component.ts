import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder } from '@angular/forms';

import { JobService } from '../../services/job.service';


@Component({
  selector: 'cc-job-add-form',
  templateUrl: './job-add-form.component.html',
  styleUrls: ['./job-add-form.component.css']
})
export class JobAddFormComponent implements OnInit {

  form: FormGroup;

  contractTypes = [
    {id: 1, name: 'stage', value: 'internship'},
    {id: 2, name: 'intérim', value: 'temp'},
    {id: 3, name: 'contrat à durée déterminée (CDD)', value: 'fixed-term'},
    {id: 4, name: 'contrat à durée indéterminée (CDI)', value: 'permanent'},
    {id: 5, name: 'indépendant', value: 'freelance'},
  ];

  areas = [
    {id: 1, name: 'aucun déplacements', value: 'none'},
    {id: 2, name: 'déplacements régionaux', value: 'region'},
    {id: 3, name: 'déplacements nationaux', value: 'nation'},
    {id: 4, name: 'déplacements internationaux', value: 'international'}
  ];


  constructor(
    private formBuilder: FormBuilder,
    private jobService: JobService
  ) { }

  ngOnInit() {
    this.form = this.formBuilder.group({
      id: -1,
      title: '',
      company: '',
      city: '',
      description: '',
      contract: '',
      salary: null,
      area: '',
      publishdate: new Date(),
    });
  }

  createJob(jobData) {
    // console.log(jobData);
    this.jobService.addJob(jobData).subscribe(res => this.form.reset());
  }

}
