import { Component, OnInit } from '@angular/core';

import { JobService } from '../../services/job.service';


@Component({
  selector: 'cc-job-list',
  templateUrl: './job-list.component.html',
  styleUrls: ['./job-list.component.css']
})
export class JobListComponent implements OnInit {

  jobs = null;
  error = '';

  constructor(private jobService: JobService) { }

  ngOnInit() {
    this.jobService.getJobs().subscribe(
      data => this.jobs = data,
      error => {
        console.error(error);
        this.error = error;
      }
    );
    this.jobService.jobsSubject.subscribe(data => {
      this.jobs = [data, ...this.jobs];
    });
  }

  deleteJob(idJob) {
    this.jobService.deleteJob(idJob).subscribe(res => {
      const job = this.jobs.filter(j => j.id === idJob);
      const index = this.jobs.indexOf(job);
      this.jobs.splice(index, 1);
    });
  }

}
